const express = require("express");
const router = express.Router();
const taskController = require("../controllers/oneTaskController.js");


// /*  Create a route for getting a specific task.
//     Create a controller function for getting a specific task.
//     Return the result back to the client/Postman.
//     Process a GET request at the /tasks/:id route using Postman to get a specific task.




router.get("/:_id", (req, res) => {
	taskController.getTask(req.params).then(resultFromController => {res.send(resultFromController)});
})



module.exports = router;
