const express = require("express");
const router = express.Router();
const taskController = require("../controllers/oneTaskController.js");


/*
    Create a route for changing the status of a task to complete.
    Create a controller function for changing the status of a task to complete.
    Return the result back to the client/Postman.
    Process a PUT request at the /tasks/:id/complete route using Postman to change the status of a task to complete.
*/

router.put("/:id/complete", (req, res) => {

	taskController.updateTask(req.params.id).then(resultFromController => {res.send(resultFromController)});
})

module.exports = router;