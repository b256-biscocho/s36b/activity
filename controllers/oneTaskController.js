const Task = require("../models/Task.js");

module.exports.getTask = (paramsId) => {
	
	return Task.findById(paramsId).then((result,err) => {
		if (result !== null){
			return result
		} else {
			return "No task found"
		}

	})
};

/*

    Create a route for changing the status of a task to complete.
    Create a controller function for changing the status of a task to complete.
    Return the result back to the client/Postman.
    Process a PUT request at the /tasks/:id/complete route using Postman to change the status of a task to complete. 
   
*/

module.exports.updateTask = (paramsId) => {
	return Task.findByIdAndUpdate(paramsId, {status : "complete"}).then((result, err) => {
		if (result.status == "complete"){
			return "Not applicable";
		} else {
			return result, `Status changed to complete`;
		}
	})
}


